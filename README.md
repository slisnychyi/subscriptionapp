# Subscriptions api

### workflow:
![alt text](subapp-workflow.png "workflow")


### frameworks/libraries in use:
- *Lombok* - for removing boilerplate code
- *Terraform* - for infrastructure organisation. E.g. aws sqs
- *Feign client* - for declarative http communication between public and subscription microservices
- *Aws client* for sqs - communication between subscription and email services
- *Spring integration* - for sqs message processing for email service  
- *JWT* token generation - for secure communication between public and subscription services (probably better to integrate some external auth server) but for simplicity use this
- *ModelMapper* - for quick DTO transformation

### how to run

- run from local enviroment
1. setup aws-keys in application.properties (subscription-service, email-service)
2. create "email-subscription" sqs in aws manually or go to email-service /deploy/aws/ and run `terraform init && terraform apply`
3. start each service separately via spring boot application

- run from docker local images 
1. setup aws-keys and aws sqs
2. run mvn clean install on each service
3. run `docker build -t ${service-name} .` on each service to create images
4. run docker-compose file from the root directory `docker-compose up`

- run from docker remote images 
1. setup aws-keys and aws sqs
2. run mvn clean install on each service
3. run `docker build -t ${service-name} .` on each service to create images
4. run `docker tag my_image $DOCKER_ID_USER/my_image` and `docker push $DOCKER_ID_USER/my_image`
5. change docker-compose file with remote images and run from the root directory `docker-compose up`

 
### ci/cd pipeline
proposed ci/cd process with jenkins pipline
1. developers send pull request to scm on each microservice
2. scm trigger webhook on: pull-request, merge to develop, merge to master
3. Build & Test: checkout code & load jenkinsfile -> compile -> static code coverage, unit tests -> integration tests -> ... other tests -> publish reports
4. Prepare container: build docker image -> tag docker image -> push docker image to registry (e.g. ecr)
5. Deploy: deploy to enviroments based on expression (e.g ecs)
6. Notification: notify teammates about deployment 
