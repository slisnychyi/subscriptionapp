package com.slisnychyi.subscriptionservice.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(value = RetentionPolicy.RUNTIME)
@Target(value = ElementType.FIELD)
@Constraint(validatedBy = BirthdayValidator.class)
public @interface ValidBirthday {

    String message() default "invalid birthday";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}