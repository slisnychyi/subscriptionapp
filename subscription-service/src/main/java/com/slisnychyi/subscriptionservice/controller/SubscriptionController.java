package com.slisnychyi.subscriptionservice.controller;

import com.slisnychyi.subscriptionservice.model.dto.User;
import com.slisnychyi.subscriptionservice.service.SubscriptionService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@Slf4j
@RestController
@Validated
@RequestMapping(value = "/api/v1/subscription",
        consumes = MediaType.APPLICATION_JSON_VALUE,
        produces = MediaType.APPLICATION_JSON_VALUE)
public class SubscriptionController {

    private final SubscriptionService subscriptionService;

    @Autowired
    public SubscriptionController(SubscriptionService subscriptionService) {
        this.subscriptionService = subscriptionService;
    }

    @PostMapping
    public String subscribe(@RequestBody @Valid User user) {
        log.info("SubscriptionService(subscribe user email = {}.", user.getEmail());
        String uuid = subscriptionService.subscribe(user);
        log.info("User was subscribed with UUID = {}", uuid);
        return uuid;
    }
}
