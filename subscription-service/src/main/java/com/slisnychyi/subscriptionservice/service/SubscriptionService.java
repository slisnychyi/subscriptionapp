package com.slisnychyi.subscriptionservice.service;

import com.slisnychyi.subscriptionservice.model.dto.User;
import com.slisnychyi.subscriptionservice.persistance.Subscription;
import com.slisnychyi.subscriptionservice.persistance.SubscriptionRepository;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

@Service
public class SubscriptionService {

    private final SubscriptionRepository subscriptionRepository;
    private final EmailNotificationService emailNotificationService;
    private final ModelMapper modelMapper;

    public SubscriptionService(SubscriptionRepository subscriptionRepository,
                               EmailNotificationService emailNotificationService,
                               ModelMapper modelMapper) {
        this.subscriptionRepository = subscriptionRepository;
        this.emailNotificationService = emailNotificationService;
        this.modelMapper = modelMapper;
    }

    public String subscribe(User user) {
        Subscription subscription = new Subscription();
        modelMapper.map(user, subscription);
        return subscriptionRepository.findByEmail(user.getEmail())
                .map(e -> e.getUuid().toString())
                .orElseGet(() -> {
                    String uuid = subscriptionRepository.save(subscription).getUuid().toString();
                    emailNotificationService.sendNotification(user);
                    return uuid;
                });
    }
}


