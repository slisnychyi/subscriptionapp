package com.slisnychyi.subscriptionservice.service;

import com.amazonaws.services.sqs.AmazonSQS;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.slisnychyi.subscriptionservice.model.dto.User;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class EmailNotificationService {

    private final AmazonSQS sqs;
    private final String sqsUrl;
    private final ObjectMapper objectMapper;

    public EmailNotificationService(@Qualifier("awsSqs") AmazonSQS sqs,
                                    @Value("${email.subscription.service.sqs.name}") String sqsName,
                                    ObjectMapper objectMapper) {
        this.sqs = sqs;
        this.objectMapper = objectMapper;
        this.sqsUrl = sqs.getQueueUrl(sqsName).getQueueUrl();
    }

    @SneakyThrows
    public void sendNotification(User user) {
        log.info("[EmailNotificationService] sending email notification for user[email={}]", user.getEmail());
        String userDetails = objectMapper.writeValueAsString(user);
        sqs.sendMessage(sqsUrl, userDetails);
    }


}
