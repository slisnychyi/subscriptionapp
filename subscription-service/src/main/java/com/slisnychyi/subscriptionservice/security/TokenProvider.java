package com.slisnychyi.subscriptionservice.security;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import io.jsonwebtoken.*;

import java.util.Base64;
import java.util.Date;

import static java.util.Objects.*;

/**
 * For simplicity use base token authorization instead of ready to use auth server provider
 */
@Slf4j
@Component
public class TokenProvider {

    private final String secret;
    private final long expirationTime;

    public TokenProvider(@Value("${jwt.token.secret}") String secret,
                         @Value("${jwt.token.expire}") long expirationTime) {
        this.secret = Base64.getEncoder().encodeToString(secret.getBytes());
        this.expirationTime = expirationTime;
    }

    public boolean isValidToken(String token) {
        Claims body;
        try {
            body = Jwts.parser().setSigningKey(secret).parseClaimsJws(token).getBody();
        } catch (Exception ex) {
            log.error("token was expired");
            return false;
        }
        String subject = body.getSubject();
        return nonNull(subject) && body.getExpiration().getTime() > new Date().getTime();
    }
}
