package com.slisnychyi.subscriptionservice.security;

import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

@Component
public class TokenFilter extends GenericFilterBean {

    private final TokenProvider provider;

    @Autowired
    public TokenFilter(TokenProvider provider) {
        this.provider = provider;
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException {
        Boolean isValidToken = Optional.of(servletRequest)
                .map(HttpServletRequest.class::cast)
                .map(e -> e.getHeader("Authorization"))
                .map(provider::isValidToken)
                .orElse(false);
        if (isValidToken) {
            filterChain(servletRequest, servletResponse, filterChain);
        } else {
            HttpServletResponse response = (HttpServletResponse) servletResponse;
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            response.getWriter().write("unauthorized");
        }
    }

    @SneakyThrows
    private void filterChain(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) {
        filterChain.doFilter(servletRequest, servletResponse);
    }
}
