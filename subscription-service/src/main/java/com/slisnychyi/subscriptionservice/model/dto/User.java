package com.slisnychyi.subscriptionservice.model.dto;

import com.slisnychyi.subscriptionservice.validation.ValidBirthday;
import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

@Data
public class User {
    @NotNull
    private String name;
    @Email
    @NotNull
    private String email;
    private String gender;
    @ValidBirthday
    private String birthday;
}
