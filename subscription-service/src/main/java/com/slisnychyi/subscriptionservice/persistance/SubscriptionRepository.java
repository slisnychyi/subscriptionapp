package com.slisnychyi.subscriptionservice.persistance;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface SubscriptionRepository extends PagingAndSortingRepository<Subscription, UUID> {

    Optional<Subscription> findByEmail(String email);

}
