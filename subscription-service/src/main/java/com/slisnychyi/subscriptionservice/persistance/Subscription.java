package com.slisnychyi.subscriptionservice.persistance;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;
import java.util.UUID;

@Entity
@Data
@EqualsAndHashCode(exclude = {"uuid", "timestamp", "email", "gender", "birthday"})
@Table(name = "Subscription")
public class Subscription {

    @Id
    @GeneratedValue
    private UUID uuid;
    @CreationTimestamp
    private LocalDateTime timestamp;
    private String name;
    private String email;
    private String gender;
    private String birthday;
}
