resource "aws_sqs_queue" "email_subscription_queue" {
  name = "email-subscription"

  tags {
    Name         = "EmailSubscriptionQueue"
    product      = "sebscription"
    microservice = "subscription-service"
  }
}