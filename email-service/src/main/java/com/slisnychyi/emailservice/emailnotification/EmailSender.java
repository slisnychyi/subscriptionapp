package com.slisnychyi.emailservice.emailnotification;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class EmailSender {

    public void sendEmail(EmailNotification emailNotification) {
        log.info("email was sent = {}", emailNotification);
    }

}
