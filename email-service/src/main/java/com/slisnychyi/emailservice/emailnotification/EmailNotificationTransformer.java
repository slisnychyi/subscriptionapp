package com.slisnychyi.emailservice.emailnotification;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessagingException;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Slf4j
@Component
public class EmailNotificationTransformer {

    private final ObjectMapper objectMapper;

    public EmailNotificationTransformer() {
        this.objectMapper = new ObjectMapper();
    }

    public EmailNotification transformMessage(Message<String> source) {
        log.info("Transforming message source to email notification message.");
        String payload = source.getPayload();
        try {
            return objectMapper.readValue(payload, EmailNotification.class);
        } catch (IOException e) {
            String message = String.format("Can't transform message=[%s]. Cause=[%s]",
                    payload, e.getMessage());
            log.error(message, e);
            throw new MessagingException(message);
        }
    }
}