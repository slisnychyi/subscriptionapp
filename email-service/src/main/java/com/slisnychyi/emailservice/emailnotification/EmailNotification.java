package com.slisnychyi.emailservice.emailnotification;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class EmailNotification {
    private String name;
    private String email;
    private String gender;
    private String birthday;
}
