package com.slisnychyi.emailservice.emailnotification;

import com.amazonaws.services.sqs.AmazonSQSAsync;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.aws.messaging.listener.SqsMessageDeletionPolicy;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.aws.inbound.SqsMessageDrivenChannelAdapter;
import org.springframework.integration.channel.QueueChannel;
import org.springframework.integration.core.MessageProducer;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;
import org.springframework.integration.scheduling.PollerMetadata;
import org.springframework.messaging.Message;
import org.springframework.messaging.PollableChannel;
import org.springframework.scheduling.support.PeriodicTrigger;

import java.util.function.Consumer;

@Slf4j
@Configuration
public class EmailNotificationIntegration {

    private static final int TRIGGER_PERIOD_MS = 1000;

    private final AmazonSQSAsync sqs;
    private final String sqsName;

    private final EmailNotificationTransformer transformer;
    private final EmailSender emailSender;

    @Autowired
    public EmailNotificationIntegration(AmazonSQSAsync sqs,
                                        @Value("${email.subscription.service.sqs.name}") String sqsName,
                                        EmailNotificationTransformer transformer, EmailSender emailSender) {
        this.sqs = sqs;
        this.sqsName = sqsName;
        this.transformer = transformer;
        this.emailSender = emailSender;
    }

    @Bean
    public PollableChannel inputChannel() {
        return new QueueChannel();
    }

    @Bean
    @SuppressWarnings("checkstyle:magicnumber")
    public MessageProducer sqsMessageDrivenChannelAdapter() {
        SqsMessageDrivenChannelAdapter adapter = new SqsMessageDrivenChannelAdapter(this.sqs, sqsName);
        adapter.setOutputChannel(inputChannel());
        adapter.setMaxNumberOfMessages(10);
        adapter.setMessageDeletionPolicy(SqsMessageDeletionPolicy.ALWAYS);
        return adapter;
    }

    @Bean
    public PollerMetadata sqsEmailNotificationPoller() {
        PollerMetadata pollerMetadata = new PollerMetadata();
        pollerMetadata.setTrigger(new PeriodicTrigger(TRIGGER_PERIOD_MS));
        pollerMetadata.setErrorHandler(t -> {
            log.error("Email Notification failed. Details=[{}]", t.getMessage());
        });
        return pollerMetadata;
    }

    @Bean
    public IntegrationFlow imageSqsFlow() {
        return IntegrationFlows.from(inputChannel())
                .transform(Message.class, transformer::transformMessage, e -> e.poller(sqsEmailNotificationPoller()))
                .handle((Consumer<EmailNotification>) emailSender::sendEmail)
                .get();
    }

}
