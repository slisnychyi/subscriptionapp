package com.slisnychyi.emailservice.configuration;

import com.amazonaws.services.sqs.AmazonSQSAsync;
import com.amazonaws.services.sqs.AmazonSQSAsyncClientBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AwsClientProvider {

    private final AwsConfiguration configuration;

    @Autowired
    public AwsClientProvider(AwsConfiguration configuration) {
        this.configuration = configuration;
    }

    @Bean
    public AmazonSQSAsync awsSqs() {
        return AmazonSQSAsyncClientBuilder.standard()
                .withCredentials(configuration.getAwsCredentialsProvider())
                .withClientConfiguration(configuration.getDefaultClientConfiguration())
                .withRegion(configuration.getRegion())
                .build();
    }
}
