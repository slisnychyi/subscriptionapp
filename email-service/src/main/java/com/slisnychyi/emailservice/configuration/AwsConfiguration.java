package com.slisnychyi.emailservice.configuration;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.retry.PredefinedRetryPolicies;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AwsConfiguration {

    private final ClientConfiguration defaultClientConfiguration;
    private final AWSCredentialsProvider awsCredentialsProvider;
    private final String region;

    public AwsConfiguration(@Value("${aws.accessKey}") String accessKey, @Value("${aws.secretKey}") String secretKey,
                            @Value("${aws.region}") String region) {
        this.region = region;
        this.defaultClientConfiguration = new ClientConfiguration().withRetryPolicy(PredefinedRetryPolicies.getDefaultRetryPolicy());
        this.awsCredentialsProvider = new AWSStaticCredentialsProvider(new BasicAWSCredentials(accessKey, secretKey));
    }

    public ClientConfiguration getDefaultClientConfiguration() {
        return defaultClientConfiguration;
    }

    public AWSCredentialsProvider getAwsCredentialsProvider() {
        return awsCredentialsProvider;
    }

    public String getRegion() {
        return region;
    }
}
