package com.slisnychyi.publicservice.models;

import com.slisnychyi.publicservice.validation.ValidBirthday;
import lombok.Data;

import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

@Data
public class User {
    @NotNull
    private String name;
    @Email
    @NotNull
    private String email;
    private String gender;
    @ValidBirthday
    private String birthday;
    @AssertTrue
    private boolean approve;
}
