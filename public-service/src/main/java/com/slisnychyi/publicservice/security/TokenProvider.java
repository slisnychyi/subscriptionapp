package com.slisnychyi.publicservice.security;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Base64;
import java.util.Date;

/**
 * For simplicity use base token authorization instead of some auth server providers
 */
@Slf4j
@Component
public class TokenProvider {

    private final String secret;
    private final long expirationTime;

    public TokenProvider(@Value("${jwt.token.secret}") String secret,
                         @Value("${jwt.token.expire}") long expirationTime) {
        this.secret = Base64.getEncoder().encodeToString(secret.getBytes());
        this.expirationTime = expirationTime;
    }

    public String generateToken(String email) {
        Claims claims = Jwts.claims().setSubject(email);
        Date now = new Date();
        return Jwts.builder()
                .setClaims(claims)
                .setIssuedAt(now)
                .setExpiration(new Date(now.getTime() + expirationTime))
                .signWith(SignatureAlgorithm.HS256, secret)
                .compact();
    }
}
