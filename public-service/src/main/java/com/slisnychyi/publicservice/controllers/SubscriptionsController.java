package com.slisnychyi.publicservice.controllers;

import com.slisnychyi.publicservice.models.User;
import com.slisnychyi.publicservice.services.SubscriptionService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.Collections;
import java.util.Map;

@Slf4j
@RestController
@RequestMapping(
        value = "/api/v1/subscriptions",
        consumes = MediaType.APPLICATION_JSON_VALUE,
        produces = MediaType.APPLICATION_JSON_VALUE)
public class SubscriptionsController {

    private static final String UUID_PARAM = "UUID";

    private final SubscriptionService subscriptionService;

    public SubscriptionsController(SubscriptionService subscriptionService) {
        this.subscriptionService = subscriptionService;
    }

    @PostMapping
    public Map<String, String> addSubscription(@RequestBody @Valid User user) {
        log.info("received user=[email={}] to subscribe", user.getEmail());
        String uuid = subscriptionService.getSubscribedUserUuid(user);
        log.info("user=[email={}] was subscribed with id=[{}]", user.getEmail(), uuid);
        return Collections.singletonMap(UUID_PARAM, uuid);
    }

}
