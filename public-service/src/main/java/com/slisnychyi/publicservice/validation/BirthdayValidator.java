package com.slisnychyi.publicservice.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import static java.time.Period.between;
import static java.util.Optional.ofNullable;

public class BirthdayValidator implements ConstraintValidator<ValidBirthday, String> {
    private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("dd/MM/yyyy");
    private static final int MIN_AGE = 18;
    private static final int MAX_AGE = 120;

    @Override
    public boolean isValid(String birthday, ConstraintValidatorContext context) {
        return ofNullable(birthday)
                .map(e -> between(LocalDate.parse(e, FORMATTER), LocalDate.now()).getYears())
                .map(years -> years >= MIN_AGE && years <= MAX_AGE)
                .orElse(false);
    }
}
