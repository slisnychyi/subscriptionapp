package com.slisnychyi.publicservice.services;

import com.slisnychyi.publicservice.integration.SubscriptionClient;
import com.slisnychyi.publicservice.models.User;
import com.slisnychyi.publicservice.security.TokenProvider;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class SubscriptionService {

    private final SubscriptionClient subscriptionClient;
    private final TokenProvider tokenProvider;

    @Autowired
    public SubscriptionService(SubscriptionClient subscriptionClient, TokenProvider tokenProvider) {
        this.subscriptionClient = subscriptionClient;
        this.tokenProvider = tokenProvider;
    }

    public String getSubscribedUserUuid(User user) {
        String token = tokenProvider.generateToken(user.getEmail());
        return subscriptionClient.subscribeUser(token, user);
    }
}
