package com.slisnychyi.publicservice.integration;

import com.slisnychyi.publicservice.models.User;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.awt.PageAttributes;

@RestController
@FeignClient(name = "subscription-service", url = "localhost:8082")
@RequestMapping(
        consumes = MediaType.APPLICATION_JSON_VALUE,
        produces = MediaType.APPLICATION_JSON_VALUE)
public interface SubscriptionClient {

    @PostMapping(value = "/api/v1/subscription")
    String subscribeUser(@RequestHeader("Authorization") String token, @RequestBody User user);

}
